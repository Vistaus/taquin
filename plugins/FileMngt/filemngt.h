/*
 * Copyright (C) 2021  Aloys Liska
 * 
 * This file is part of Taquin.
 * 
 * Taquin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * Taquin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Taquin.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef FILEMNGT_H
#define FILEMNGT_H

#include <QObject>
#include <QDir>
#include <QFile>
#include <QFileInfo>
#include <QStandardPaths>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonValue>
#include <QJsonArray>
#include <QVariant>


// class for file management
class FileMngt: public QObject {
    Q_OBJECT

public:
    FileMngt();
    ~FileMngt() = default;

    // return true if file named "source" exists, false otherwise
    Q_INVOKABLE bool exists(QString source);    
    
    // return the complete base name of the file named "source", return empty string if file does not exist
    Q_INVOKABLE QString fileCompleteBaseName(QString source);   
    
    // remove file named "source" and return true, or return false is file does not exist
    Q_INVOKABLE bool remove(QString source);
    
    // rename file named "source" with name "filename" and return true, or return false is file does not exist
    Q_INVOKABLE bool rename(QString source, const QString& fileName);
    
    // return suffix of file named "source", return empty string if file does not exist
    Q_INVOKABLE QString suffix(QString source);
    
    // json object and file management
    Q_INVOKABLE bool loadJsonGameState(QString key, bool defaultVal);   // load and return value at key from gameDataObj, or return defaultVal is gameDataObj is empty
    Q_INVOKABLE int  loadJsonGameState(QString key, int defaultVal);    // load and return value at key from gameDataObj, or return defaultVal is gameDataObj is empty
    Q_INVOKABLE QVariant loadJsonGameState(QString key);                // load and return array at key from gameDataObj, return 0 value if error
    Q_INVOKABLE bool saveJsonGameState(QString key, bool val);          // save val at key in gameDataObj
    Q_INVOKABLE bool saveJsonGameState(QString key, int val);           // save val at key in gameDataObj
    Q_INVOKABLE bool saveJsonGameState(QString key, QVariantList val);  // save array at key in gameDataObj
    Q_INVOKABLE bool saveJsonFile();                                    // save gameDataObj in json file, return false if fail

private:
    // path and filename for app data json file
    QString const appDataPath = QStandardPaths::writableLocation(QStandardPaths::AppDataLocation);
    QString const appDataFileName = "taquinGameState.json";
    
    // json object containing game state
    QJsonObject gameDataObj;
    
    // loading of json file into gameDataObj
    bool loadJsonFile();
};


#endif
