# Taquin
A puzzle game for Ubuntu Touch.

Taquin is the classic puzzle where you have to place tiles in order by sliding them.
All tiles are initially in a random order, and there is one tile missing creating an empty space.
The objective is to re-organize the tiles by moving them. Only the tiles next to the empty space can be moved.

You can configure the game to change the number of rows and columns.
You can also select displaying numbers or an image among three or select your own image from another application.
With a 4 rows and 4 columns, this is the well-known 15-puzzle game, which name in french is "Taquin".

You can find Taquin release in the OpenStore: https://open-store.io/app/taquin.aloysliska


## License

Copyright (C) 2022  Aloys Liska

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License version 3, as published
by the Free Software Foundation.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranties of MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.


## How to build
In a desktop Linux distribution, use clickable tool: <https://clickable-ut.dev/en/latest/#>


## About the 15 puzzle game
Some useful links:

 - wikipedia page: https://en.wikipedia.org/wiki/15_puzzle
 - a good video for the maths behind the game: https://youtu.be/YI1WqYKHi78 (especially to understand the "parity")

 
## About Author and the application development
This is my first application developped for Ubuntu Touch: the Mobile Version of the Ubuntu Operating System (see <https://ubports.com/>)

My environment development:

 - Kubuntu 20.04
 - Editor: kate
 - builder: clickable
 - other tools: git and "Git Cola" for git GUI

The sources are in QML, javascript and C++.

The docs I used:

- Ubuntu Touch documentation: https://docs.ubports.com/en/latest/appdev/index.html
- Qt Programming course from mimecar: https://mimecar.gitbook.io/qt-course/
- The Ubuntu Touch API documentation: https://api-docs.ubports.com/sdk/apps/qml/index.html
- QML, starting with: https://doc.qt.io/qt-5/qml-tutorial1.html
- javascript: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide
- SQL with SQLite: https://sqlite.org/lang.html
 


