/*
 * Copyright (C) 2021  Aloys Liska
 * 
 * This file is part of Taquin.
 * 
 * Taquin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * Taquin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Taquin.  If not, see <http://www.gnu.org/licenses/>.
 */
 
import QtQuick 2.7
import Qt.labs.platform 1.0
import Lomiri.Components 1.3
import Lomiri.Content 1.3

import FileMngt 1.0

Page {
    id: imgImportPage
    
    property var activeTransfer
    property string imgSuffix
    property string imgFileBaseName 
        
    visible: false
    header: PageHeader {
        id: headerInfo
        title: i18n.tr("Image Selection")
    }
    
    // Define a ContentStore to store the imported image
    ContentStore {
        id: imgStore
        scope: ContentScope.App
    }    

    // Lomiri.Content ContentPeerPicker
    // Allow user selection of application (peer) which will be the source for image (picture)
    ContentPeerPicker {
        anchors.fill: parent
        anchors.topMargin: imgImportPage.header.height
        visible: parent.visible
        showTitle: false
        contentType: ContentType.Pictures
        handler: ContentHandler.Source

        onPeerSelected: {
            peer.selectionType = ContentTransfer.Single
            imgImportPage.activeTransfer = peer.request(imgStore)   // request transfer and storage in imgStore
        }
        
        onCancelPressed: {
            pageStack.pop()
        }
    }
    
    // Display a waiting screen during transfer
    ContentTransferHint {
        id: transferHint
        anchors.fill: parent
        activeTransfer: imgImportPage.activeTransfer
    }
    
    Connections {
        target: imgImportPage.activeTransfer
        onStateChanged: {
            
            switch (imgImportPage.activeTransfer.state) {
                case ContentTransfer.Created:
                    console.log("Transfer Created")
                    break;
                    
                case ContentTransfer.Initiated:
                    console.log("Transfer Initiated")
                    break;
                    
                case ContentTransfer.InProgress:
                    console.log("Transfer InProgress")
                    break;
                    
                case ContentTransfer.Downloading:
                    console.log("Transfer Downloading")
                    break;
                    
                case ContentTransfer.Downloaded:
                    console.log("Transfer Downloaded")
                    break;
                    
                case ContentTransfer.Charged:
                    console.log("Transfer Charged")
                    console.log("Tranfered image url:", imgImportPage.activeTransfer.items[0].url)
                    
                    if (FileMngt.exists(myImageUrl)) {
                        // The previous imported image file exists
                        // Get the filename
                        imgFileBaseName = FileMngt.fileCompleteBaseName(myImageUrl)
                        
                        // Update filename
                        // This is a trick to cause refresh of image display in main view
                        // the trick is to have two file name: filename1 and filename2.
                        // by switching between these two filenames, the source for image (QML component) will change and cause the reload of image
                        if (imgFileBaseName == "UserImportedImg1")
                            imgFileBaseName = "UserImportedImg2"
                        else
                            imgFileBaseName = "UserImportedImg1"
                        
                        // Delete previous imported image
                        if (FileMngt.remove(myImageUrl)) {
                            console.log("File deleted : ", myImageUrl)
                        }                    
                        else {
                            console.log("ERROR - FAIL TO DELETE: ", myImageUrl)
                        }
                    }
                    else {
                        // The previous imported image file does not exist
                        // Define filename
                        imgFileBaseName = "UserImportedImg1"
                    }
                    
                    // Get the file suffix of the imported image
                    imgSuffix = FileMngt.suffix(imgImportPage.activeTransfer.items[0].url)
                    
                    // Rename the file name of imported image in filename1 or filename2
                    if (FileMngt.rename(imgImportPage.activeTransfer.items[0].url, imgStore.uri + "/" + imgFileBaseName + "." + imgSuffix)) {
                        // Update myImageUrl, this will cause update of the image source in Tile.qml
                        myImageUrl = imgStore.uri + "/" + imgFileBaseName + "." + imgSuffix
                        indexImg = indexMyImage // value to display user image
                        console.log("File renamed to:", myImageUrl)
                    }
                    else {
                        console.log("ERROR - FAIL TO RENAME FILE to: ", myImageUrl)
                    }
                    
                    break;
                    
                case ContentTransfer.Collected:
                    console.log("Transfer Collected")
                    imgImportPage.activeTransfer.finalize()
                    pageStack.pop()
                    break;
                    
                case ContentTransfer.Aborted:
                    console.log("Transfer Aborted")
                    break;
                    
                case ContentTransfer.Finalized:
                    console.log("Transfer Finalized")
                    break;
                    
                default:
                    console.log("Transfer Unkonwn State")
            }            
        }
    }
    
    Component.onDestruction: {
            console.log("Destruction imgImportPage")
    }       
}
