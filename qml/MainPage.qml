/*
 * Copyright (C) 2021  Aloys Liska
 * 
 * This file is part of Taquin.
 * 
 * Taquin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * Taquin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Taquin.  If not, see <http://www.gnu.org/licenses/>.
 */


import QtQuick 2.7
import Lomiri.Components 1.3
import QtQuick.Layouts 1.3
import QtQuick.LocalStorage 2.7
import Qt.labs.settings 1.0

import "taquin.js" as Taquin
import "statdb.js" as Statdb

Page {
    id: mainPage
    visible: false
    
    Component.onDestruction: {  
        console.log("Destruction MainPage")
    }
    
    header: PageHeader {
        id: headerMainPage
        title: i18n.tr('Taquin')
        
        contents: Item { 
            id: mainHeaderText
            anchors.fill: parent
            Label {
                id: labelTitle
                text: i18n.tr('Taquin')
                textSize: Label.XLarge
                anchors.verticalCenter: parent.verticalCenter
                anchors.left: parent.left
                anchors.leftMargin: units.gu(2)
            }
            Item {  // use item to center Label between labelTitle and mainHeaderActionBar
                anchors.verticalCenter: parent.verticalCenter
                anchors.left: labelTitle.right
                anchors.right: parent.right
                height: parent.height
                
                Label {
                    width: parent.width
                    height: parent.height
                    anchors.centerIn: parent
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                    wrapMode: Text.Wrap
                    elide: Text.ElideRight
                    
                    property string winText
                    
                    text: {
                        if (gameOver)
                            winText = '. ' + i18n.tr('WIN!')
                        else
                            winText = ""

                        if (movcount > 1) {
                            // plural text
                            return (movcount + ' ' + i18n.tr('moves') + winText)
                        }
                        else {
                            // singular text
                            return (movcount + ' ' + i18n.tr('move') + winText)
                        }
                    }
                }
            }
        }
        
        trailingActionBar {
            id: mainHeaderActionBar
            actions: [
                Action {
                    iconName: "insert-image"
                    text: i18n.tr('Import image')
                    onTriggered: {
                        pageStack.push(Qt.resolvedUrl("ImgImportPage.qml"))
                    }   
                },
                Action {
                    iconName: "reload"
                    text: i18n.tr('New game')
                    onTriggered: {
                        pageStack.newGame()
                    }   
                },
                Action {
                    iconName: "settings"
                    text: i18n.tr('Settings')
                    onTriggered: {
                        pageStack.push(Qt.resolvedUrl("SettingsPage.qml"))
                    }
                },
                Action {
                    iconName: "document-preview"
                    text: i18n.tr('Statistics')
                    onTriggered: {
                        pageStack.push(Qt.resolvedUrl("StatPage.qml"))
                    }  
                },
                Action {
                    iconName: "info"
                    text: i18n.tr('Information')
                    onTriggered: {
                        pageStack.push(Qt.resolvedUrl("InfoPage.qml"))
                    }  
                }
            ]
            numberOfSlots: 3
        }
    }
    
    Item {
        id:itemTable
        property var imageHeight: parent.height - headerMainPage.height
        
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.top: headerMainPage.bottom
        anchors.topMargin: {
            if (squareTile == true) 
                return ((imageHeight - itemTable.height)/2)     // to center vertically
            else
                return 0
        }
        
        height: { 
            if (squareTile == true) {
                if (imageHeight * maxCol > parent.width * maxRow)   // Tile height > Tile width
                    return (parent.width * maxRow / maxCol)         // use Tile width 
                else
                    return imageHeight
            }
            else
                return imageHeight
        }
        width:  {
            if (squareTile == true) {
                if (imageHeight * maxCol < parent.width * maxRow)   // Tile width > Tile height
                    return (imageHeight * maxCol / maxRow)          // use Tile height
                else
                    return parent.width
            }
            else
                return parent.width
        } 
        
        // double click in empty cell, will display numbers in the tiles
        MouseArea {
            anchors.fill: parent
            
            onDoubleClicked: {
                dispNumb = !dispNumb
            }
        }
        
        // The core of taquin: the matrix of tiles
        Repeater {
            id: mouleAgaufre
            model: { 
                //console.log("model=", tabTsize)     
                return tabTsize
            }
            Tile { 
                n: index
                tileVal: tileTable[index]
                
                property int tmp: 0
                
                onActionTile: {
                    tmp = n
                    Taquin.moveSelectedTile(coord, movTile)
                    if ((movTile[0] != 0) || (movTile[1] != 0)) {
                        mouleAgaufre.itemAt(initFreeIndex).n = tmp     // to move the free tile
                    }
                    
                    // check game over at each tile move and save game statistics if game over
                    if (!gameOver) {
                        if (Taquin.checkForCompleted()) {
                            gameOver = true
                            if (movcount != 0) {
                                console.log("SAVE WIN GAME. gameOver true AND movCount !=0")
                                Statdb.SaveStatsinDB(gameOver)
                            }
                        }
                            
                    }
                    if (gameOver)
                        mouleAgaufre.itemAt(initFreeIndex).visible = true   // make visible the free tile
                }
            }
            
            Component.onCompleted: {
                gameOver = Taquin.checkForCompleted() // check gameover for new game (in case already won by chance)
                if (gameOver)
                    mouleAgaufre.itemAt(initFreeIndex).visible = true   // make visible the free tile
            }
        }
    }
}
