/*
 * Copyright (C) 2021  Aloys Liska
 * 
 * This file is part of Taquin.
 * 
 * Taquin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * Taquin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Taquin.  If not, see <http://www.gnu.org/licenses/>.
 */
 
import QtQuick 2.7
import QtGraphicalEffects 1.12
import Lomiri.Components 1.3

Page {
    id: infoPage
    visible: false
    header: PageHeader {
        id: headerInfo
        title: i18n.tr('App Information')
    }

    Flickable {
        anchors.top: headerInfo.bottom
        width: parent.width
        height: parent.height - headerInfo.height
        contentHeight: infoColumn.height
    
        Column {
            id: infoColumn
            spacing: units.gu(2)
            width: parent.width
            topPadding: units.gu(2)
            
            // Application Icon
            Item {
                id: appIcon
                anchors.horizontalCenter: parent.horizontalCenter
                height: Math.min(parent.width/4, parent.height/4)
                width:height

                Icon {
                    id: theicon
                    width: appIcon.width
                    height:appIcon.height
                    name:"Taquin"
                    source:Qt.resolvedUrl("../assets/logo.svg")
                    visible: false
                }

                Rectangle {
                    id: mask
                    width: appIcon.width
                    height:appIcon.height
                    border.width: 0
                    radius: appIcon.height * 0.25
                    visible: false
                }

                // to have icon in a rectangle with rounded corner
                OpacityMask {
                    anchors.fill: theicon
                    source: theicon
                    maskSource: mask
                }
            }

            // Application Name
            Label {
                width: parent.width
                color: theme.palette.normal.backgroundText
                horizontalAlignment: Text.AlignHCenter
                font.bold: true
                textSize: Label.XLarge
                text: i18n.tr("Taquin")
            }
            
            // Application Version
            Label {
                width: parent.width
                color: theme.palette.normal.backgroundSecondaryText
                horizontalAlignment: Text.AlignHCenter
                textSize: Label.Large
                text: i18n.tr("Version %1").arg("2.1.1")
            }
            
            // Application short description
            Label {
                width: parent.width - 2*anchors.leftMargin  // width set to have appropriate text wrapping
                anchors.left: parent.left
                anchors.leftMargin: units.gu(4)
                color: theme.palette.normal.backgroundSecondaryText
                wrapMode: Text.Wrap
                text: i18n.tr("Taquin is the classic puzzle where you have to place the tiles in order by sliding them. " 
                            + "Slide the tile you want to move. You can move only a tile next to the empty space.")
            }
            
            Grid {
                anchors.left: parent.left
                anchors.leftMargin: units.gu(4)
                columns: 2
                spacing: units.gu(1)

                // Application Author 
                Label {
                    color: theme.palette.normal.backgroundTertiaryText
                    font.bold: true
                    text: i18n.tr("Author:")
                }
                Label {
                    color: theme.palette.normal.backgroundTertiaryText
                    text: "<a href=\"https://gitlab.com/AloysLiska\">Aloys Liska</a><br>"
                    onLinkActivated: Qt.openUrlExternally(link) 
                } 
                
                // Application Contributors 
                Label {
                    color: theme.palette.normal.backgroundTertiaryText
                    font.bold: true
                    text: i18n.tr("Translators:")
                }
                Label {
                    color: theme.palette.normal.backgroundTertiaryText
                    wrapMode: Text.Wrap
                    text: "<a href=\"https://gitlab.com/Anne17\">Anne Onyme 017</a> (fr)<br>"
                        + "<a href=\"https://gitlab.com/Vistaus\">Heimen Stoffels</a> (nl)<br>"
                        + "<a href=\"https://gitlab.com/theBest3000/\">Wolfgang Eder</a> (de)<br>"
                    onLinkActivated: Qt.openUrlExternally(link) 
                }    
            }
            
            Grid {
                anchors.left: parent.left
                anchors.leftMargin: units.gu(4)
                columns: 2
                spacing: units.gu(1)
                
                // Source Code
                Label {
                    color: theme.palette.normal.backgroundTertiaryText
                    font.bold: true
                    text: i18n.tr("Source code:")
                }
                Label {
                    color: theme.palette.normal.backgroundTertiaryText
                    text: "<a href=\"https://gitlab.com/ubuntu-touch-applications/taquin\">GitLab</a>"
                    onLinkActivated: Qt.openUrlExternally(link)
                }
                
                //Bug tracker
                Label {
                    color: theme.palette.normal.backgroundTertiaryText
                    font.bold: true
                    text: i18n.tr("Bug tracker:")
                }
                Label {
                    color: theme.palette.normal.backgroundTertiaryText
                    text: "<a href=\"https://gitlab.com/ubuntu-touch-applications/taquin/-/issues\">GitLab</a>"
                    onLinkActivated: Qt.openUrlExternally(link)
                }
                
                // License
                Label {
                    color: theme.palette.normal.backgroundTertiaryText
                    font.bold: true
                    text: i18n.tr("License:")
                }
                Label {
                    color: theme.palette.normal.backgroundTertiaryText
                    text: "<a href=\"http://www.gnu.org/licenses/gpl.txt\">GNU GPL v3</a><br>"
                    onLinkActivated: Qt.openUrlExternally(link)
                }
            }
            
            // Application Copyright
            Label {
                width: parent.width
                color: theme.palette.normal.backgroundTertiaryText
                horizontalAlignment: Text.AlignHCenter
                text: i18n.tr("Copyright (c) 2022: %1").arg("Aloys Liska")
            }
        }
    }
    
    Component.onDestruction: {
        console.log("Destruction infoPage")
    }        
}
