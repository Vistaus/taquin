/*
 * Copyright (C) 2022  Aloys Liska
 * 
 * This file is part of Taquin.
 * 
 * Taquin is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * Taquin is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Taquin.  If not, see <http://www.gnu.org/licenses/>.
 */


/*******************************/
/* SQLite Data Base Management */
/*******************************/

/*
 * openStatDataBase()
 * Create or open the data base "taquinStat_db"
 */
function openStatDataBase() {
    return LocalStorage.openDatabaseSync("taquinStat_db", "1.0", "Statistics of Taquin", 1000000);
}


/*
 * CreateDBtable()
 * Create table tqStat if it does not exist
 * Table is composed of columns:
 *  - key: primary key used to identify a row from the table. It is build by concatenating the numbers maxRow & maxCol.
 *         Concatenation is defined in integer by: key = maxRow * 10^( quotient(maxCol/10) + 1) + maxRow
 *  - tqrow: maxRow taquin number of rows
 *  - tqcolumn: maxCol taquin number of column
 *  - winG: total number of won games
 *  - cancelG: total number of canceld games
 *  - minWinMov: minimum number of moves in won games 
 *  - maxWinMov: maximum number of moves in won games
 *  - totalWinMov: Total number of moves in won games
 *  - totalCancelMov: total number of moves in canceled games
 * 
 */
function CreateDBtable() {
    var db = openStatDataBase();
    db.transaction(
        function(tx) {
            // Create the database if it doesn't already exist
            tx.executeSql('CREATE TABLE IF NOT EXISTS tqStat(key INTEGER PRIMARY KEY, tqrow INTEGER, tqcolumn INTEGER, winG INTEGER, cancelG INTEGER, minWinMov INTEGER, maxWinMov INTEGER, totalWinMov INTEGER, totalCancelMov INTEGER)');
        }
    )
}


/*
 * SaveStatsinDB(type)
 *  - type: false for a canceled game
 *          true for a won game
 * 
 * Save in the database the statistics for a canceled or won game.
 * 1) if it does not exists, insert row in data base with current game (defined by key) 
 * 2) update values in the row (of database) corresponding to the current game (defined by key)
 */
function SaveStatsinDB(type) {
    // Define a key for the database by concatenating maxRow and maxCol
    var key = maxRow * 10 * (Math.floor(maxCol/10) + 1) + maxCol
    
    var db = openStatDataBase();

    // 1) if it does not exists, insert row in data base with current game (defined by key) 
    db.transaction(
        function(tx) {
            // Insert a row with key if it does not exist
            var checkRow = tx.executeSql('SELECT key FROM tqStat WHERE key = ?', [ key ]);
            if (checkRow.rows.length == 0) {
                tx.executeSql('INSERT INTO tqStat VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?)', [ key, maxRow, maxCol, 0, 0, 0, 0, 0, 0 ]);
            }
        }
    )
    
    // 2) update values in the row (of database) corresponding to the current game (defined by key)
    if (type == false)
    {
        // update for a canceled game
        db.transaction(
            function(tx) {
                // increment cancelG
                tx.executeSql('UPDATE tqStat SET cancelG = cancelG + 1 WHERE key = ?', [ key ]);
                
                // add movcount to totalCancelMov
                tx.executeSql('UPDATE tqStat SET totalCancelMov = totalCancelMov + ? WHERE key = ?', [ movcount, key ]);
            }
        )
    }
    else {
        // update for a won game
        db.transaction(
            function(tx) {
                // increment winG
                tx.executeSql('UPDATE tqStat SET winG = winG + 1 WHERE key = ?', [ key ]);
                
                // add movcount to totalWinMov
                tx.executeSql('UPDATE tqStat SET totalWinMov = totalWinMov + ? WHERE key = ?', [ movcount, key ]);
                
                // update minWinMov
                var cval = tx.executeSql('SELECT minWinMov FROM tqStat WHERE key = ?', [ key ]);
                if (cval.rows.item(0).minWinMov == 0) {
                    // exclude value zero from minimum value, if zero update with movcount
                    tx.executeSql('UPDATE tqStat SET minWinMov = ? WHERE key = ?', [ movcount, key ]);
                }
                else {
                    // update with minimum value
                    tx.executeSql('UPDATE tqStat SET minWinMov = min(minWinMov,?) WHERE key = ?', [ movcount, key ]);
                }
                // update maxWinMov
                tx.executeSql('UPDATE tqStat SET maxWinMov = max(maxWinMov,?) WHERE key = ?', [ movcount, key ]);
            }
        )
    }
}

/*
 * ReadStatsfromDB(n)
 * Read stats from the data base
 * parameters: 
 *  - n: maximum of elements to be output from the database
 */
function ReadStatsfromDB(n) {
    var db = openStatDataBase();
    var rnum = []
    
    db.transaction(
        function(tx) {
            // get current game
            var rsC = tx.executeSql('SELECT * FROM tqStat WHERE tqrow = ? AND tqcolumn = ?', [ maxRow, maxCol ]);
            if (rsC.rows.length > 0) {
                rnum.push(rsC.rows.item(i).key);
                rnum.push(rsC.rows.item(i).tqrow);
                rnum.push(rsC.rows.item(i).tqcolumn);
                rnum.push(rsC.rows.item(i).winG);
                rnum.push(rsC.rows.item(i).cancelG);
                rnum.push(rsC.rows.item(i).minWinMov);
                rnum.push(rsC.rows.item(i).maxWinMov);
                rnum.push(rsC.rows.item(i).totalWinMov);
                rnum.push(rsC.rows.item(i).totalCancelMov);
            }
            
            // get all games sorted by win.
            var rs = tx.executeSql('SELECT * FROM tqStat WHERE tqrow != ? OR tqcolumn != ? ORDER BY winG DESC LIMIT 0,?', [ maxRow, maxCol, n ]);
            for (var i = 0; i < rs.rows.length; i++) {
                rnum.push(rs.rows.item(i).key);
                rnum.push(rs.rows.item(i).tqrow);
                rnum.push(rs.rows.item(i).tqcolumn);
                rnum.push(rs.rows.item(i).winG);
                rnum.push(rs.rows.item(i).cancelG);
                rnum.push(rs.rows.item(i).minWinMov);
                rnum.push(rs.rows.item(i).maxWinMov);
                rnum.push(rs.rows.item(i).totalWinMov);
                rnum.push(rs.rows.item(i).totalCancelMov);
            }
        }
    )
    return rnum;
}
